#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#define tgn 5
#define tgs 5
#define tge 2.5f
#define tgw 2.5f
#define tcross 3
#define tsafety 7
#define PORTB (1<<1)
#define PORTC (1<<2)
#define PORTD (1<<3)
#define HIGH 1
#define LOW 0
#define RED 0x2;
#define BLUE 0x4;
#define GREEN 0x8;
#define WHITE 0xE;
// flags 
char isNSgreen, isEWgreen, isSafe, hasLeft = 0;								// status for junctions and train
char ped = 0;																											// status for pedestrians

// semaphores 
xSemaphoreHandle xTrainEnteredSemaphore;											// semaphore for train gate (sensor 1)
xSemaphoreHandle xTrainExitSemaphore;													// semaphore for train gate (sensor 2) 

// functions
void PortF_Init(void);																				// a function to initialize port F 
void PortE_Init(void);																				// a function to initiallie port E
void GPIO_PORT_Init(int port);
void GPIO_setBitVal(int port,int pin,int val);
void NorthSouthGo();
void NorthSouthStop();
void EastWestGo();
void EastWestStop();
void GPIOF_Handler(void);																			// handler for on board switches 1 and 2
void GPIOE_Handler(void);																			// handler for external switch interrupt 
void delayMs(int mis);																				// delay function in Milliseconds
void PWM_Init();
void raiseGate();
void dropGate();
void init_PWM_Siren();
void start_siren();
void stop_siren();
// Tasks 
static void vTaskTrainEntered(void* pvParameters);						// a task to close gate
static void vTaskTrainExit(void* pvParameters);							  // a task to manage junctions while a pedestrian is passing
static void vTaskNS( void *pvParameters );									  // a task to conrol North South junctions
static void vTaskEW( void *pvParameters );								    // a task to conrol East West junctions
static void vTaskPedestrian(void *pvParameters);					    // a task to control junctions in case of pedestrians

// Task Handles 
xTaskHandle NS_Handle;                                        // Handle to reference NorthSouth task
xTaskHandle EW_Handle;																				// Handle to reference EastWest task
xTaskHandle Ped_Handle;																				// Handle to reference Pedestrian task





int main(void)
{
	
	//enable global interrupts (CPSID I for disable)
  __asm("CPSIE  I");
	
	// Initiallize ports 
  
	PWM_Init();
	PortF_Init();  																							// PortF initialize 
	PortE_Init();																								// PortE initialize
	GPIO_PORT_Init(PORTB);
	GPIO_PORT_Init(PORTC);
	GPIO_PORT_Init(PORTD);
	init_PWM_Siren();
	// create semaphores 
	vSemaphoreCreateBinary( xTrainEnteredSemaphore );
	vSemaphoreCreateBinary( xTrainExitSemaphore );

	// check semaphore created 
	if ( (xTrainEnteredSemaphore != NULL) & (xTrainExitSemaphore != NULL) )
	{
		// create Tasks 
		xTaskCreate(vTaskTrainEntered, (const portCHAR *)"TrainEnteredHandler", configMINIMAL_STACK_SIZE, NULL, 5, NULL);
		xTaskCreate(vTaskTrainExit, (const portCHAR *)"TrainExitHandler", configMINIMAL_STACK_SIZE, NULL, 6, NULL);
		xTaskCreate(vTaskPedestrian, (const portCHAR *)"PedestrianHandler", configMINIMAL_STACK_SIZE, NULL, 2, &Ped_Handle);
		
		xTaskCreate( vTaskNS, (const portCHAR *)"TaskNS", configMINIMAL_STACK_SIZE, NULL, 3, &NS_Handle );
		xTaskCreate( vTaskEW, (const portCHAR *)"TaskEW", configMINIMAL_STACK_SIZE, NULL, 2, &EW_Handle );
	
		// start scheduler 
		vTaskStartScheduler();
	}
	


}

// Implementing Functions 
void init_PWM_Siren(){
    //clock for PWM1
    SYSCTL_RCGCPWM_R |= (1 << 1); //enable pwm module 1 ds:353
	//Clock for PORT E
    SYSCTL_RCGCGPIO_R |= (1 << 4);
	
	SYSCTL_RCC_R = 0x00140000;      //PWM Clock and divdier , clock frequency= 2MHz
		SYSCTL_RCGC0_R |= (1<<20); 
	
	GPIO_PORTE_DR4R_R|=(1<<5);
	//Data direction
    GPIO_PORTE_DIR_R |= (1 << 5);
	//Digital EN
    GPIO_PORTE_DEN_R |= (1 << 5);
    //AFSEL for PortE pin 5
    GPIO_PORTE_AFSEL_R |= (1 << 5);
    
    
    //Assign the PWM signals to specific pins using GPIOCTL register.
    GPIO_PORTE_PCTL_R = 0x00500000;
    //Disable generator by PWMxCTL register
    PWM1_1_CTL_R &= ~(1 << 0);
    PWM1_1_CTL_R &= ~(1 << 1);
    //Configure PWMxGENA
    PWM1_1_GENB_R |= 0x8C;
    //Load Value into PWMxLOAD (output frequency)
    PWM1_1_LOAD_R = 40000;
    //Load value into PWMxCMPA (duty cycle)
    //value compared against the counter
    PWM1_1_CMPA_R = 20000;
    //Start PWM generator using PWMxCTL
    //PWM1_1_CTL_R |= (1 << 0);
    //configure PWMxENABLE to direct PWMx to output pin
    //PWM1_ENABLE_R |=(1<<3) ;
    }
void start_siren(){
	  GPIO_PORTE_DEN_R |= (1 << 5);
    //Start PWM generator using PWMxCTL
    PWM1_1_CTL_R |= (1 << 0);
    //configure PWMxENABLE to direct PWMx to output pin
    PWM1_ENABLE_R |= (1<<3);
}
void stop_siren(){
		GPIO_PORTE_DEN_R &= ~(1 << 5);
    //Start PWM generator using PWMxCTL
    PWM1_1_CTL_R &= ~(1 << 0);
}
void PWM_Init()
{
	SYSCTL_RCGCPWM_R |= 1;       /* enable clock to PWM0 */
  SYSCTL_RCGCGPIO_R |= (1<<1);		//clock for PortB
	int volatile delay;
	  SYSCTL_RCC_R = 0x00140000;      //PWM Clock and divdier , clock frequency= 2MHz
	//SYSCTL_RCC_R &= ~(SYSCTL_RCC_USEPWMDIV); 
		SYSCTL_RCGC0_R |= (1<<20);         
	  //SYSCTL_RCGC2_R |= (1<<1);         //PWM Module 1
	//delay=SYSCTL_RCGC2_R;

  GPIO_PORTB_DR4R_R|=(1<<6);
	GPIO_PORTB_DIR_R|=(1<<6);
	GPIO_PORTB_DEN_R |= (1<<6);        // PB6 output 
  GPIO_PORTB_AFSEL_R |= (1<<6);      // alternating function PB6
  GPIO_PORTB_PCTL_R = 0x04000000;   // PWM Pin Out
}
void dropGate()     
{
	start_siren();
  PWM0_0_CTL_R &=~(1<<0);
	PWM0_0_CTL_R &=~(1<<1);
	PWM0_0_GENA_R|=0x8C;
	PWM0_0_LOAD_R=40000;
	PWM0_0_CMPA_R=38250;
	PWM0_0_CTL_R|=0x1;
	PWM0_ENABLE_R|=PWM_ENABLE_PWM0EN;
}

void raiseGate()      // rotate anticlockwise
{
	stop_siren();
  PWM0_0_CTL_R &=~(1<<0);
	PWM0_0_CTL_R &=~(1<<1);
	PWM0_0_GENA_R|=0x8C;
	PWM0_0_LOAD_R=40000;
	PWM0_0_CMPA_R=39000;
	PWM0_0_CTL_R|=0x1;
	PWM0_ENABLE_R|=PWM_ENABLE_PWM0EN;
}
void PortF_Init(void)
{ 
		// activate clock for port F
		SYSCTL_RCGCGPIO_R |= 0x00000020;

		// unlock switch 2 
		GPIO_PORTF_LOCK_R = 0x4C4F434B;																// unlock commit register 
		GPIO_PORTF_CR_R = 0x1;																			 // make port f configurable 
		GPIO_PORTF_LOCK_R = 0;																			// relock commit register 
	
		// set directions for port f 
		GPIO_PORTF_DIR_R |=  (1<<1) | (1<<2)| (1<<3);   					// RGB leds (output) 
		GPIO_PORTF_DIR_R &= ~0x11;															 // sw1 and sw2 (input)

		// digital enable 
		GPIO_PORTF_DEN_R |= 0x1F;

		// enable pull up resitor for sw1 and sw2
		GPIO_PORTF_PUR_R |= (1<<4) | (1<<0);
	
		// make pin edge sensitive 
		GPIO_PORTF_IS_R &= ~ 0x11;
	
		// interrupt conrolled by interrupt event 
		GPIO_PORTF_IBE_R &= ~0x11;

		// falling edge trigger
		GPIO_PORTF_IEV_R &= ~0x11;
		
		// clear prior interrupts 
		GPIO_PORTF_ICR_R |= 0x11;
		
		// unmask interrupt 
		GPIO_PORTF_IM_R |= 0x11;
	
		// set priority 
		NVIC_PRI7_R = 6<<21;
		
		// enable IRQ30
		NVIC_EN0_R |= 0x40000000;	
}

void PortE_Init(void)
{
	//activate clock for port E 
	SYSCTL_RCGCGPIO_R |= (1<<4);
	
	// make PE4 as input 
	GPIO_PORTE_DIR_R &= ~(1<<4);

	// digital enable PE4 
	GPIO_PORTE_DEN_R |= (1<<4);
	
	// pull up resistor for external switch (sw3)
	GPIO_PORTE_PUR_R |= (1<<4);

	// make pin edge sensitive 
	GPIO_PORTE_IS_R &= ~ (1<<4);
	
	// interrupt conrolled by interrupt event 
	GPIO_PORTE_IBE_R &= ~(1<<4);

	// falling edge trigger
	GPIO_PORTE_IEV_R &= ~(1<<4);
		
	// clear prior interrupts 
	GPIO_PORTE_ICR_R |= (1<<4);
		
	// unmask interrupt 
	GPIO_PORTE_IM_R |= (1<<4);
	
	// set priority 
	NVIC_PRI1_R = 7<<5;
		
	// enable IRQ4
	NVIC_EN0_R |= 0x10;	
}

void GPIO_PORT_Init(int port)
{
	int dummy;
	SYSCTL_RCGCGPIO_R|=port;
	dummy=SYSCTL_RCGCGPIO_R;
	switch(port)
	{
		case PORTB:
			GPIO_PORTB_DIR_R|=0x1C;
			GPIO_PORTB_DEN_R|=0x1C;
			GPIO_PORTB_DATA_R=0x00;
			break;
		case PORTC:
			GPIO_PORTC_DIR_R|=0xF0;
			GPIO_PORTC_DEN_R|=0xF0;
			GPIO_PORTC_DATA_R=0x00;
			break;
		case PORTD:
			GPIO_PORTD_DIR_R|=0xC0;
			GPIO_PORTD_DEN_R|=0xC0;
			GPIO_PORTD_DATA_R=0x00;
			break;
		default:
			break;
	}
}
void GPIO_setBitVal(int port,int pin,int val)
{
	switch(port)
	{
		case PORTB:
			if(val)
				GPIO_PORTB_DATA_R|=(1<<pin);
			else
				GPIO_PORTB_DATA_R&=~(1<<pin);
			break;
		case PORTC:
			if(val)
				GPIO_PORTC_DATA_R|=(1<<pin);
			else
				GPIO_PORTC_DATA_R&=~(1<<pin);
			break;
		case PORTD:
			if(val)
				GPIO_PORTD_DATA_R|=(1<<pin);
			else
				GPIO_PORTD_DATA_R&=~(1<<pin);
			break;
		default:
			break;
	}
}
void NorthSouthGo()
{
	GPIO_setBitVal(PORTB,3,HIGH);
	GPIO_setBitVal(PORTB,2,LOW);
	GPIO_setBitVal(PORTC,5,HIGH);
	GPIO_setBitVal(PORTC,4,LOW);
	
}
void NorthSouthStop()
{
	GPIO_setBitVal(PORTB,3,LOW);
	GPIO_setBitVal(PORTB,2,HIGH);
	GPIO_setBitVal(PORTC,5,LOW);
	GPIO_setBitVal(PORTC,4,HIGH);
}
void EastWestGo()
{
	GPIO_setBitVal(PORTC,7,HIGH);
	GPIO_setBitVal(PORTC,6,LOW);
	GPIO_setBitVal(PORTB,4,HIGH);
	GPIO_setBitVal(PORTD,6,LOW);
}
void EastWestStop()
{
	GPIO_setBitVal(PORTC,7,LOW);
	GPIO_setBitVal(PORTC,6,HIGH);
	GPIO_setBitVal(PORTB,4,LOW);
	GPIO_setBitVal(PORTD,6,HIGH);
}
void GPIOF_Handler(void)
{
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	
	while(GPIO_PORTF_MIS_R  != 0)
	{
		// check if sw1
		if(GPIO_PORTF_MIS_R & 0x10)
		{
				// give semaphore to unblock task  
				xSemaphoreGiveFromISR( xTrainEnteredSemaphore, &xHigherPriorityTaskWoken );
	
				// clear interrupt on PF4
				GPIO_PORTF_ICR_R |= (1<<4);
	
				// switch context 
				portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
		}
		// check if sw2
		else if(GPIO_PORTF_MIS_R & 0x01)
		{
				// give semaphore to unblock task  
				xSemaphoreGiveFromISR( xTrainExitSemaphore, &xHigherPriorityTaskWoken );
	
				// clear interrupt on PF0
				GPIO_PORTF_ICR_R |= (1<<0);
	
				// switch context 
				portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
		}		
	}	
}

void GPIOE_Handler(void)
{
	// Raise pedestrian flag 
	ped = 1;
	
	
	// give semaphore to unblock task  
	
	//clear interrupt 
	GPIO_PORTE_ICR_R |= (1<<4);

	// switch context 
}

static void vTaskTrainEntered(void* pvParameters )
{	
	//take sempahore 
	xSemaphoreTake(xTrainEnteredSemaphore, 0);
	
	// loop indefentily
	for(;;)
	{
		// flag has left down 
		hasLeft = 0;
		
		// not safe 
		isSafe = 0;
		
		xSemaphoreTake(xTrainEnteredSemaphore, portMAX_DELAY);
		
		// red LED on (to be replaced by turning junctions red)
		GPIO_PORTF_DATA_R = (1<<1);
		NorthSouthStop();
		EastWestStop();
		dropGate();
		
		// wait for tsafety 
		delayMs(tsafety * 1000);
		
		// safe now 
		isSafe = 1;
		
		// busy wait on train until left
		while(hasLeft == 0);
		raiseGate();
		if(isNSgreen)
			{
				// turn NS green 
				NorthSouthGo();
				GPIO_PORTF_DATA_R = BLUE;
			}
			else if(isEWgreen)
			{
				// turn EW green 
				EastWestGo();
				GPIO_PORTF_DATA_R = GREEN;
			}
	}
}

static void vTaskTrainExit(void* pvParameters)
{
	//take sempahore 
	xSemaphoreTake(xTrainExitSemaphore, 0);
	
	// loop indefentily
	for(;;)
	{
		xSemaphoreTake(xTrainExitSemaphore, portMAX_DELAY);
		hasLeft = 1;
		// check if tsafety has passed
		if(isSafe)
		{					
			// check if NS was green 
			raiseGate();
			if(isNSgreen)
			{
				// turn NS green 
				NorthSouthGo();
				GPIO_PORTF_DATA_R = BLUE;
			}
			else if(isEWgreen)
			{
				// turn EW green 
				EastWestGo();
				GPIO_PORTF_DATA_R = GREEN;
			}
		}
	}
}

void delayMs(int mis)
{
  int i ,j;
  for(i = 0 ; i<2*mis ;i++)
  {
    for(j=0;j<397;j++);
  }
}

static void vTaskNS( void *pvParameters )
{	
	UBaseType_t uxPriority=uxTaskPriorityGet(NULL);//3
	for( ;; )
	{		
			// flag for NS green (true)
			isNSgreen = 1;
		
			//Blue LED used for testing to be replaced with proper pin outs
			GPIO_PORTF_DATA_R = BLUE;
			NorthSouthGo();
		  EastWestStop();
			delayMs(tgn*1000);//5 seconds
		
			// check if pedestrian has passed
			if(ped)
			{
				// raise pedestrian task priority to be the highest of NS and EW
				vTaskPrioritySet(Ped_Handle,(uxPriority+1));     													// priority became 4 
			}				
		
			// flag for NS red (false)
			isNSgreen = 0;
		
			vTaskPrioritySet(EW_Handle,(uxPriority+1));//3
	}
}

static void vTaskEW( void *pvParameters )
{
	UBaseType_t uxPriority=uxTaskPriorityGet(NULL);//3
	for( ;; )
	{			
			// flag for NS green (true)
			isEWgreen = 1;
		
			//Green LED used for testing; to be replaced with proper pin outs
			GPIO_PORTF_DATA_R = GREEN;
			NorthSouthStop();
			EastWestGo();		
			delayMs(tge*1000);//2.5 seconds
		
			if(ped)
			{
				// raise pedestrian task priority to be the highest of NS and EW
				vTaskPrioritySet(Ped_Handle,uxTaskPriorityGet(NS_Handle)+1);     													// priority became 4 				
			}
	
			// flag for NS green (true)
			isEWgreen = 0;
		
			vTaskPrioritySet(NULL,(uxPriority-2));//1
	}
}

static void vTaskPedestrian(void *pvParameters)
{
	// get current task priority 
	UBaseType_t uxPriority=uxTaskPriorityGet(NULL);
	
	//take sempahore 
	
	// loop indefentily
	for(;;)
	{
		
		// red LED on (to be subtituted with suitable pinouts)
		GPIO_PORTF_DATA_R =WHITE ;
		NorthSouthStop();
		EastWestStop();
		
		// delay for tcross seconds
		delayMs(tcross*1000);
		// pedestrian flag down
		ped =0;
		//decrease current task's priority to be the lowest of NS EW
		if(isNSgreen){
		vTaskPrioritySet(NULL, uxPriority -2);					// priority became 1
		}
		else
		{
			vTaskPrioritySet(NULL, uxPriority -3);	
		}
		
		
	}
}
